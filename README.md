# EWU Wordpress theme generator
The purpose of this script is make it easy to produce consistent blocks of code for use in the ewutheme_s wordpress theme.

## Usage
**Command structure:** `php generate {{What to generate}} "Name To Generate"`

### Example command:
This command uses the /test/generate.php command, which will work in this repository but is not exported for use through composer (the package manager). Ideally you would copy /test/generate.php to a local file in the repository you wish to use this in and update the output paths to match your structure.

**Sample:** `php test/generate.php module "Main Nav"

The above script will output a module folder with the name of `main-nav` in the specified output directory defined in the /test/generate.php file. Since the script is generating a module it uses the module output path.

## Feature planing
- Add support for widgets
- Add support for custom post types
- Possibly split out template interpolation into it's own, more abstract package

