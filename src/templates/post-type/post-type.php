<?php
$label = '{{name}}';
$plurl = $label.'s';
$slug = '{{processedName}}';

register_post_type($slug, array(
        'label' => $label,
        'description' => '',
        'public' => true,
        'show_in_rest' => true,
        'exclude_from_search' => false,
        'publicly_queryable'  => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'menu_icon' => 'dashicons-admin-generic',
        'capability_type' => 'post',
        'map_meta_cap' => true,
        'hierarchical' => true,
        'rewrite' => array('with_front' => false, 'slug' => $slug),
        'query_var' => true,
        'can_export' => true,
        'has_archive' => false,
        'supports' => array('title', 'editor', 'thumbnail'),
        'acf' => true,
        'labels' => array (
            'name' => $plurl,
            'singular_name' => $label,
            'menu_name' => $plurl,
            'add_new' => 'Add '.$label,
            'add_new_item' => 'Add New '.$label,
            'edit' => 'Edit',
            'edit_item' => 'Edit '.$label,
            'new_item' => 'New '.$label,
            'view' => 'View '.$label,
            'view_item' => 'View '.$label,
            'search_items' => 'Search '.$plurl,
            'not_found' => 'No '.$label.' Found',
            'not_found_in_trash' => 'No '.$label.' Found in Trash',
            'parent' => 'Parent '.$label,
        )
    )
);
