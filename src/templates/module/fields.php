<?php

$key = '{{key}}';

$fields = [
    "available_as_content_block" => false,
    "title" => "{{name}}",
    "name" => "{{processedName}}",
    "key" => $key,
    "fields" => [
        [
            "label" => "My fields",
            "name" => "field_name",
            "key" => $key."_field_name",
            "required" => true,
            "type" => "text",
        ]
    ]
];

return $fields;