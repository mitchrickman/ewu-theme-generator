<?php

namespace EwuThemeGenerator;

require_once __DIR__.'/includes/utilities.php';

foreach ( glob(__DIR__.'/generators/*.php') as $file ) {
    require $file;
}

class Generate {
    public function generate( $output ) {
        $command_line_arguments = $_SERVER['argv'];
        $what_to_generate = $command_line_arguments[1];
        $name = $command_line_arguments[2];
        $name_processed = \Utilities\hyphenate($name);
        
        $generateable_items = [
            'module',
            'post-type'
        ];

        if ( !in_array($what_to_generate, $generateable_items) ) {
            echo \Utilities\invalid_generateable_error( $generateable_items );
            die();
        }

        
        switch( $what_to_generate ) {
            case "module":
                $module = new ModuleGenerator( $output['module'], $name );
                $module->generate();
                break;
            case "post-type":
                $post_type = new PostTypeGenerator( $output['post-type'], $name );
                $post_type->generate();
                break;
        }
    }
}