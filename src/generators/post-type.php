<?php

namespace EwuThemeGenerator;

require_once __DIR__.'/../includes/utilities.php';

class PostTypeGenerator {
    private $output = '';
    private $name = '';
    private $processed_name = '';
    private $class_name = '';
    private $template_directory = __DIR__.'/../templates/post-type/';
    private $unique_key = '';

    public function __construct($output, $name) {
        $this->output = $output;
        $this->name = $name;
        $this->processed_name = \Utilities\hyphenate( $this->name );
    }

    public function generate() {
        $this->create_post_type_directory_or_error();
       
        $template_files = glob($this->template_directory.'*');

        // Exit with error if template is missing
        if ( !$template_files ) {
            echo 'Template not found in : ' . $this->template_directory;
            return;
        }
        
        foreach ( $template_files as $template_file ) {
            $file_name = explode('/', $template_file);
            $file_name = array_pop( $file_name );
            $file_name = trim($file_name);
            $file_name = str_replace('post-type', $this->processed_name,  $file_name);
            $template_content = ''.file_get_contents($template_file);

            $template_fields = [
                'name' => $this->name,
                'processedName' => $this->processed_name,
            ];

            $template_content = \Utilities\insert_into_template( $template_content, $template_fields );

            $new_file = file_put_contents($this->output.'/'.$file_name, $template_content);

            echo "file name: ".$file_name."\r\n";
        }
    }

    private function create_post_type_directory_or_error() {
        if ( file_exists($this->output.$this->processed_name.'.php') ) {
            echo 'Post type already exists: ' . $this->name;
            die();
        } 
        else {
            mkdir($this->output, 0777, true);
        }
    }
}