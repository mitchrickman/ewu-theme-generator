<?php

namespace EwuThemeGenerator;

require_once __DIR__.'/../includes/utilities.php';

class ModuleGenerator {
    private $output = '';
    private $name = '';
    private $processed_name = '';
    private $class_name = '';
    private $template_directory = __DIR__.'/../templates/module/';
    private $unique_key = '';

    public function __construct($output, $name) {
        $this->output = $output;
        $this->name = $name;
        $this->processed_name = \Utilities\hyphenate( $this->name );
        $this->class_name = \Utilities\camelCase( $this->name );
        $this->unique_key = md5(microtime().rand());
    }

    public function generate() {
        $this->create_module_directory_or_error();
       
        $template_files = glob($this->template_directory.'*');

        // Exit with error if template is missing
        if ( !$template_files ) {
            echo 'Template not found in : ' . $this->template_directory;
            return;
        }
        
        foreach ( $template_files as $template_file ) {
            $file_name = explode('/', $template_file);
            $file_name = array_pop( $file_name );
            $file_name = trim($file_name);
            $file_name = str_replace('module', $this->processed_name,  $file_name);
            $template_content = ''.file_get_contents($template_file);

            $template_fields = [
                'name' => $this->name,
                'processedName' => $this->processed_name,
                'className' => $this->class_name,
                'key' => $this->unique_key,
            ];

            $template_content = \Utilities\insert_into_template( $template_content, $template_fields );

            $new_file = file_put_contents($this->output.$this->processed_name.'/'.$file_name, $template_content);

            echo "file name: ".$file_name."\r\n";
        }
    }

    private function create_module_directory_or_error() {
        if ( file_exists($this->output.$this->processed_name) ) {
            echo 'Module already exists: ' . $this->name;
            die();
        } 
        else {
            mkdir($this->output.$this->processed_name, 0777, true);
        }
    }
}