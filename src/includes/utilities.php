<?php

namespace Utilities;

/**
 * return invalid generate type error
 *
 * @param array $generateable_items
 * @return void
 */
function invalid_generateable_error( $generateable_items ) {
    $message = "\r\n\r\n";
    $message .= "Don't know what to generate, please follow this format for your command \r\n\r\n";
    $message .= "php generate WHAT_TO_GENERATE \"NAME OF THE GENERATED ITEM\" \r\n\r\n";

    $message .= "Generatables: \r\n";
    
    foreach ( $generateable_items as $item ) {
        $message .= "• $item \r\n";
    }

    $message .= "\r\n\r\n";

    return $message;
}

/**
 * Lower case string and replace whitespace with "-"
 *
 * @param string $string
 * @return string
 */
function hyphenate( $string ) {
    $hyphenated_string = preg_replace( '!\s+!', ' ', $string );
    $hyphenated_string = str_replace( ' ', '-', $hyphenated_string );
    
    return strtolower( $hyphenated_string );
}

function camelCase( $string ) {
    $output_string = preg_replace( '!\s+!', ' ', $string );
    $output_string = ucwords($output_string);
    
    return str_replace( ' ', '', $output_string );
}

/**
 * Replace variable instance with setting
 *
 * @param string $content
 * @param object $settings
 * @return void
 */
function insert_into_template( $content, $settings ) {
    foreach ( $settings as $field => $value ) {
        $content = preg_replace('/{{'.$field.'}}/', $value, $content);
    }

    return $content;
}