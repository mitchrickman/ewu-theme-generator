<?php
require __DIR__.'/../src/index.php';

// clean the output directory
system('rm -rf '.__DIR__.'/output');
mkdir( __DIR__.'/output/' );

$output = [
    'module' => __DIR__.'/output/modules/',
    'post-type' => __DIR__.'/output/post-type/',
];

$generator = new EwuThemeGenerator\Generate;
$generator->generate( $output );

